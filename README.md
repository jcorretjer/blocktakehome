## Build tools & versions used
* Jetpack Nav Component(latest version) - helps you implement navigation, from simple button clicks to more complex patterns, such as app bars and the navigation drawer
* Lifecycle(latest version) - allows performing actions in response to a change in the lifecycle status of another component, such as activities and fragments. These components help you produce better-organized, and often lighter-weight code, that is easier to maintain. 
* Kotlin Coroutines(latest version) - a concurrency design pattern that you can use on Android to simplify code that executes asynchronously. 
* Dagger Hilt/Testing(latest version) - a dependency injection library for Android that reduces the boilerplate of doing manual dependency injection in your project. 
* Retrofit(latest version) - A type-safe HTTP client for Android and Java.
* Glide(lastest version) - a fast and efficient image loading library for Android focused on smooth scrolling. Offers memory and disk caching, by default.
* MVVM design pattern - a software architectural pattern that facilitates the separation of the development of the graphical user interface from the development of the business logic so that the UI is not dependent on any specific model platform.

Apart from what's above, everything else is auto-included every time a project is created.

## Steps to run the app
Nothing out of the ordinary. Hit "play" or "debug" from Android Studio and that's it. 

## Steps to test
Some test cases require specific configuration changes to pass. For example, no internet connection tests will fail if the device is not in an offline state(or airplane mode). More details about this can be found documented in the `DataTest` interface.

## What areas of the app did you focus on?
Only on what was specified in the take-home document. Which are:
* Consume a JSON web response for a directory of employees and display that data in some sort of list.
* The UI must present an image, name, and team.
* The list must have some sort of way to refresh.
* The network architecture must be able to handle "load, empty y error" states.
* Use a placeholder if there was some sort of error.
* Cache the employee image.
* Include unit tests and instrumentation tests are not required
* Exclude malformed employee data from the list
* Include a read me with the provided content in the requirements doc.

## What was the reason for your focus? What problems were you trying to solve?
Unapplicable. Only focused on what was requested and nothing more.

## How long did you spend on this project?
Roughly 6-7 hours. Not in one sitting.

## Did you make any trade-offs for this project? What would you have done differently with more time?
No compromises were made. The architecture used in this project is the same one used on every application I've written.

## What do you think is the weakest part of your project?
At first, none so far. After a walk around a local woodsy park, it hit me. The loading state was not properly handled and thus wasn't visually representing the data being loaded. As soon as I realized this I ran home and fixed my mistake. Beyond that, I've used the same type of code/architecture on all my projects and it hasn't given me any problems when scaling or maintaining it. I'm also always looking for ways to improve my trade. So, if there are improvement suggestions I'll gladly review them and find a way to adapt them.

## Did you copy any code or dependencies? Please make sure to attribute them here!
I did not use any 3rd party dependencies or pieces of code written by someone else. If I did, I would've left a note somewhere. Give credit where credit is due.

## Is there any other information you’d like us to know?
Nope. No additional information is required.
