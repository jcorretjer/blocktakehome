package com.retroroots.blocktakehome.data.remote

import com.retroroots.blocktakehome.common.DataTest
import com.retroroots.blocktakehome.data.common.datasource.apiresponse.ApiResponse
import com.retroroots.blocktakehome.data.common.datasource.apiresponse.error.ApiError
import com.retroroots.blocktakehome.data.employeedir.EmployeeDirRepo
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class EmployeeDirRepoTest : DataTest
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var fakeEmployeeDirRepo: EmployeeDirRepo

    @Before
    fun init()
    {
        hiltRule.inject()
    }

    @Test
    override fun validateResponseWasSuccessfulTest()
    {
        runBlocking {
            assertTrue(fakeEmployeeDirRepo.getAllEmployees() is ApiResponse.Success)
        }
    }

    @Test
    override fun validateResponseWasUnSuccessfulBecauseNoInternetTest()
    {
        runBlocking {
            val response = fakeEmployeeDirRepo.getAllEmployees()

            assertTrue(response is ApiResponse.Error && response.error is ApiError.NoInternetConnectionError)
        }
    }

    @Test
    override fun validateServerErrorFromEmptyDataTest()
    {
        runBlocking {

            val response = fakeEmployeeDirRepo.getAllEmployees()

            assertTrue(response is ApiResponse.Error && response.error is ApiError.ServerError)
        }
    }
}