package com.retroroots.blocktakehome.data.remote.di

import android.content.Context
import com.retroroots.blocktakehome.data.employeedir.remote.api.EmployeeDirectoryApi
import com.retroroots.blocktakehome.data.employeedir.remote.di.EmployeeDirModule
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.Employee
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeeresponse.EmployeeDirectoryResponse
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeeresponse.EmployeeResponse
import com.retroroots.blocktakehome.util.networkmanager.NetworkUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import retrofit2.Response
import java.net.UnknownHostException
import javax.inject.Singleton

@Module
@TestInstallIn(components = [SingletonComponent::class], replaces = [EmployeeDirModule::class])
object FakeEmployeeDirModule
{
    val fakeEmployees = EmployeeDirectoryResponse(listOf(
        EmployeeResponse(
            "bio",
            "email",
            Employee.Type.FULL_TIME,
            "name",
            "num",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/small.jpg",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/large.jpg",
            "team",
            "id"
        ),

        EmployeeResponse(
            "bio",
            null,
            Employee.Type.PART_TIME,
            null,
            "num",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/small.jpg",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/large.jpg",
            null,
            "id2"
        ),

        EmployeeResponse(
            "bio",
            "",
            Employee.Type.FULL_TIME,
            "",
            "num",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/small.jpg",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/large.jpg",
            "",
            "i3"
        ),

        EmployeeResponse(
            "bi",
            "email",
            Employee.Type.CONTRACTOR,
            "name",
            "num",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394",
            "team2",
            "id4"
        ),

        EmployeeResponse(
            biography =  "bio",
            employeeType = Employee.Type.PART_TIME,
            phoneNumber = "num",
            photoUrlLarge = "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/small.jpg",
            photoUrlSmall = "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/large.jpg",
            uuid = "id5"
        )
    ))

    //val fakeEmployees = EmployeeDirectoryResponse(emptyList())

    @Singleton
    @Provides
    fun providesFakeFakeApi(@ApplicationContext context: Context) = object : EmployeeDirectoryApi
    {
        override suspend fun getAllEmployees(): Response<EmployeeDirectoryResponse> =
                if(NetworkUtil.isConnected(context))
                    Response.success(200, fakeEmployees)
                else
                    throw UnknownHostException()

    }
}