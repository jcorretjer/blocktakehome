package com.retroroots.blocktakehome.common

interface DataTest
{
    /**
     * Disconnect device from internet or it will fail
     */
    fun validateResponseWasSuccessfulTest()

    /**
     * Disconnect device from internet or it will fail
     */
    fun validateResponseWasUnSuccessfulBecauseNoInternetTest()

    /**
     * Change the value of fakeEmployees to empty in FakeEmployeeDirModule or this test will fail
     */
    fun validateServerErrorFromEmptyDataTest()
}