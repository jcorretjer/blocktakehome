package com.retroroots.blocktakehome.ui

import com.retroroots.blocktakehome.common.DataTest
import com.retroroots.blocktakehome.data.common.datasource.apiresponse.ApiResponse
import com.retroroots.blocktakehome.data.common.datasource.apiresponse.error.ApiError
import com.retroroots.blocktakehome.data.employeedir.EmployeeDirRepo
import com.retroroots.blocktakehome.ui.mainfragment.MainFragmentViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class MainFragmentViewModelTest : DataTest
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var fakeEmployeeDirRepo: EmployeeDirRepo

    private lateinit var mainFragmentViewModel: MainFragmentViewModel

    @Before
    fun init()
    {
        hiltRule.inject()

        mainFragmentViewModel = MainFragmentViewModel(fakeEmployeeDirRepo)
    }

    @Test
    override fun validateResponseWasSuccessfulTest()
    {
        runBlocking {
            assertTrue(mainFragmentViewModel.getAllEmployees().take(2).last() is ApiResponse.Success)
        }
    }

    @Test
    override fun validateResponseWasUnSuccessfulBecauseNoInternetTest()
    {
        runBlocking {
            val response = mainFragmentViewModel.getAllEmployees().first() as ApiResponse.Error

            assertTrue(response.error is ApiError.NoInternetConnectionError)
        }
    }

    @Test
    override fun validateServerErrorFromEmptyDataTest()
    {
        runBlocking {

            val response = mainFragmentViewModel.getAllEmployees().take(2).last()

            assertTrue(response is ApiResponse.Error && response.error is ApiError.ServerError)
        }
    }

    @Test
    fun validateInitialDataStateIsLoadingTest() {
        runBlocking {
            Assert.assertTrue(mainFragmentViewModel.getAllEmployees().first() is ApiResponse.Loading)
        }
    }
}