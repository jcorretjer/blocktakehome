package com.retroroots.blocktakehome.util.networkmanager

import android.content.Context

interface NetworkManager
{
    fun isConnected(context: Context) : Boolean
}