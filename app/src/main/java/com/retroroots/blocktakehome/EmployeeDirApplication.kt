package com.retroroots.blocktakehome

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class EmployeeDirApplication : Application()