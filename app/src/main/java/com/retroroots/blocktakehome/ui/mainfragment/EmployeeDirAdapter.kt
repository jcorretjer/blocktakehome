package com.retroroots.blocktakehome.ui.mainfragment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.retroroots.blocktakehome.R
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.Employee

class EmployeeDirAdapter(private val context: Context, private val onItemClickedListener: OnItemClickedListener) : ListAdapter<Employee, EmployeeDirAdapter.EmployeeDirViewHolder>(DiffCallback())
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            EmployeeDirViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.employee_item_layout, parent, false)
            )


    override fun onBindViewHolder(holder: EmployeeDirViewHolder, position: Int)
    {
        getItem(position).let {

            holder.apply {
                Glide.with(context)
                    .load(it.photoUrlSmall)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_account_circle_24)
                    .error(R.drawable.ic_baseline_broken_image_24)
                    .fallback(R.drawable.ic_baseline_broken_image_24)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(employeeImgVw)

                employeeFullNameTxtVw.text = it.fullName

                employeeTeamTxtVw.text = it.team

                itemView.setOnClickListener {_->
                    onItemClickedListener.onItemClicked(it)
                }
            }
        }
    }

    inner class EmployeeDirViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val employeeImgVw: ImageView = view.findViewById(R.id.employeeImgVw)

        val employeeFullNameTxtVw: TextView = view.findViewById(R.id.employeeFullNameTxtVw)

        val employeeTeamTxtVw: TextView = view.findViewById(R.id.employeeTeamTxtVw)
    }

    class DiffCallback : DiffUtil.ItemCallback<Employee>()
    {
        override fun areItemsTheSame(
            oldItem: Employee,
            newItem: Employee
        ): Boolean
        {
            return oldItem.uuid == newItem.uuid
        }

        override fun areContentsTheSame(
            oldItem: Employee,
            newItem: Employee
        ): Boolean
        {
            return oldItem == newItem
        }
    }

    interface OnItemClickedListener
    {
        fun onItemClicked(employee: Employee)
    }
}