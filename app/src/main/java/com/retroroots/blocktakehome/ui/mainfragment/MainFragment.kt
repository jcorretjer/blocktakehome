package com.retroroots.blocktakehome.ui.mainfragment

import android.os.Bundle
import android.view.View
import android.view.ViewStub
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.retroroots.blocktakehome.R
import com.retroroots.blocktakehome.data.common.datasource.apiresponse.ApiResponse
import com.retroroots.blocktakehome.data.common.datasource.apiresponse.error.ApiError
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.Employee
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainFragment : Fragment(R.layout.fragment_main)
{
    private val mainFragmentViewModel by viewModels<MainFragmentViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        val swipeRefreshLayout = view.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout)

        //Adapter
        val employeeDirAdapter = EmployeeDirAdapter(requireContext(), object: EmployeeDirAdapter.OnItemClickedListener
        {
            override fun onItemClicked(employee: Employee)
            {
                findNavController().navigate(MainFragmentDirections.actionMainFragmentToEmployeeDetailFragment(employee))
            }

        })

        val recyclerView = view.findViewById<RecyclerView>(R.id.employeeRecVw).apply {
            setHasFixedSize(true)

            adapter = employeeDirAdapter
        }

        view.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout).apply {
            setOnRefreshListener {
                mainFragmentViewModel.requestAllEmployees()
            }
        }

        val noInternetVw = view.findViewById<ViewStub>(R.id.noInternetVw).apply {
            inflate()

            visibility = View.GONE
        }

        val emptyVw = view.findViewById<ViewStub>(R.id.emptyVw).apply {
            inflate()

            visibility = View.GONE
        }

        val loadingVw = view.findViewById<ViewStub>(R.id.loadingVw).apply {
            inflate()

            visibility = View.GONE
        }

        val errorVw = view.findViewById<ViewStub>(R.id.errorVw).apply {
            inflate()

            visibility = View.GONE
        }

        //Consume apis
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED)
            {
                //Bind employee data to views
                mainFragmentViewModel.getAllEmployees().collectLatest {

                    when (it)
                    {
                        is ApiResponse.Success ->
                        {
                            updateViewVisibilitiesByResponseState(
                                Pair(noInternetVw,  null),
                                Pair(emptyVw,  null),
                                Pair(loadingVw,  null),
                                Pair(errorVw,  null),
                                Pair(recyclerView, View.VISIBLE)
                            )

                            employeeDirAdapter.submitList(it.data)
                        }

                        is ApiResponse.Error ->
                        {
                            it.error.apply {
                                when (this)
                                {
                                    is ApiError.NoInternetConnectionError ->
                                    {
                                        updateViewVisibilitiesByResponseState(
                                            Pair(noInternetVw, View.VISIBLE),
                                            Pair(emptyVw,  null),
                                            Pair(loadingVw,  null),
                                            Pair(errorVw,  null),
                                            Pair(recyclerView,  null)
                                        )

                                        println(this.exception.message)
                                    }

                                    is ApiError.ServerError ->
                                    {
                                        updateViewVisibilitiesByResponseState(
                                            Pair(noInternetVw,  null),
                                            Pair(emptyVw, View.VISIBLE),
                                            Pair(loadingVw,  null),
                                            Pair(errorVw,  null),
                                            Pair(recyclerView,  null)
                                        )

                                        println(this.exception.message)
                                    }

                                    is ApiError.UnknownServerError ->
                                    {
                                        updateViewVisibilitiesByResponseState(
                                            Pair(noInternetVw,  null),
                                            Pair(emptyVw,  null),
                                            Pair(loadingVw,  null),
                                            Pair(errorVw, View.VISIBLE),
                                            Pair(recyclerView,  null)
                                        )

                                        println(this.exception.message)
                                    }
                                }
                            }
                        }

                        else ->
                        {
                            if (!swipeRefreshLayout.isRefreshing)
                            {
                                updateViewVisibilitiesByResponseState(
                                    Pair(noInternetVw, null),
                                    Pair(emptyVw,  null),
                                    Pair(loadingVw, View.VISIBLE),
                                    Pair(errorVw,  null),
                                    Pair(recyclerView,  null)
                                )
                            }

                            else
                                swipeRefreshLayout.isRefreshing = false
                        }
                    }
                }
            }
        }
    }

    /**
     * @param views  First, view to update. Second, visibility constant. Set second to null to set visibility GONE.
     */
    private fun updateViewVisibilitiesByResponseState(vararg views : Pair<View, Int?>)
    {
        views.forEach {
            it.second?.let { s ->
                it.first.visibility = s
            }?: kotlin.run {
                it.first.visibility = View.GONE
            }
        }
    }
}