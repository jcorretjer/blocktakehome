package com.retroroots.blocktakehome.ui.mainfragment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.retroroots.blocktakehome.data.common.datasource.apiresponse.ApiResponse
import com.retroroots.blocktakehome.data.common.datasource.apiresponse.error.ApiError
import com.retroroots.blocktakehome.data.employeedir.EmployeeDirRepo
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.Employee
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainFragmentViewModel @Inject constructor (private val employeeDirRepo: EmployeeDirRepo): ViewModel()
{
    private val mutableEmployees : MutableStateFlow<ApiResponse<List<Employee>, ApiError>> = MutableStateFlow(ApiResponse.Loading)

    init
    {
        requestAllEmployees()
    }

    fun getAllEmployees() : StateFlow<ApiResponse<List<Employee>, ApiError>> = mutableEmployees

    fun requestAllEmployees()
    {
        mutableEmployees.value = ApiResponse.Loading

        viewModelScope.launch(Dispatchers.IO) {
            employeeDirRepo.getAllEmployees().collect {
                mutableEmployees.value = it
            }
        }
    }
}