package com.retroroots.blocktakehome

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class EmployeeDetailFragment : Fragment(R.layout.fragment_employee_detail)
{
    private val employeeInfo : EmployeeDetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        val details = employeeInfo.employeeInfo

        view.findViewById<TextView>(R.id.bioTxtVw).text = details.biography

        view.findViewById<TextView>(R.id.emailTxtVw).text = details.emailAddress

        view.findViewById<TextView>(R.id.employeeTypeTxtVw).text = details.employeeType.value

        view.findViewById<TextView>(R.id.nameTxtVw).text = details.fullName

        view.findViewById<TextView>(R.id.phoneNumTypeTxtVw).text = details.phoneNumber

        Glide.with(requireContext())
            .load(details.photoUrlLarge)
            .centerCrop()
            .placeholder(R.drawable.ic_baseline_account_circle_24)
            .error(R.drawable.ic_baseline_broken_image_24)
            .fallback(R.drawable.ic_baseline_broken_image_24)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view.findViewById(R.id.photoImgVw))

        view.findViewById<TextView>(R.id.teamTxtVw).text = details.team
    }
}