package com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeeresponse


import com.google.gson.annotations.SerializedName
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.Employee

data class EmployeeResponse(
    @SerializedName("biography")
    val biography: String = "",
    @SerializedName("email_address")
    val emailAddress: String? = "",
    @SerializedName("employee_type")
    val employeeType: Employee.Type = Employee.Type.FULL_TIME,
    @SerializedName("full_name")
    val fullName: String? = "",
    @SerializedName("phone_number")
    val phoneNumber: String = "",
    @SerializedName("photo_url_large")
    val photoUrlLarge: String = "",
    @SerializedName("photo_url_small")
    val photoUrlSmall: String = "",
    @SerializedName("team")
    val team: String? = "",
    @SerializedName("uuid")
    val uuid: String = ""
)