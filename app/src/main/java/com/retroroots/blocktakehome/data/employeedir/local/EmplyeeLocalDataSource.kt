package com.retroroots.blocktakehome.data.employeedir.local

import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.Employee

class EmplyeeLocalDataSource(private val  employeeDao: EmployeeDao)
{
    fun getEmployee() = employeeDao.getEmployee()

    suspend fun insertEmployee(employeeEntity: List<Employee>)
    {
        employeeDao.insert(employeeEntity)
    }
}