package com.retroroots.blocktakehome.data.employeedir.local.di

import androidx.room.RoomDatabase
import com.retroroots.blocktakehome.data.employeedir.local.EmployeeDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object EmployeeModule
{
    @Singleton
    @Provides
    fun providesEmployeeDB(employeeDbBuilder: RoomDatabase.Builder<EmployeeDB>) = employeeDbBuilder.build()

    @Singleton
    @Provides
    fun providesEmployeeDao(employeeDB: EmployeeDB) = employeeDB.employeeDao()
}