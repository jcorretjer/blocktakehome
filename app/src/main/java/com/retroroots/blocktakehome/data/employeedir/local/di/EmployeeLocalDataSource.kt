package com.retroroots.blocktakehome.data.employeedir.local.di

import com.retroroots.blocktakehome.data.employeedir.local.EmployeeDao
import com.retroroots.blocktakehome.data.employeedir.local.EmplyeeLocalDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object EmployeeLocalDataSource
{
    @Singleton
    @Provides
    fun providesEmployeeLocalDataSource(employeeDao: EmployeeDao) = EmplyeeLocalDataSource(employeeDao)
}