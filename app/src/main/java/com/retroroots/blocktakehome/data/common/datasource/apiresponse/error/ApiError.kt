package com.retroroots.blocktakehome.data.common.datasource.apiresponse.error

import java.lang.Exception

sealed class ApiError
{
    data class NoInternetConnectionError(override val exception: Exception) : Error, ApiError()

    companion object
    {
        const val SERVER_ERROR_MESSAGE = "server error"
    }

    data class ServerError(override val exception: Exception) : Error, ApiError()

    data class UnknownServerError(override val exception: Exception) : Error, ApiError()
}
