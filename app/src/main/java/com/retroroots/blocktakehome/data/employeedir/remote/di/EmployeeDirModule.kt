package com.retroroots.blocktakehome.data.employeedir.remote.di

import com.retroroots.blocktakehome.data.employeedir.remote.api.EmployeeDirectoryApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object EmployeeDirModule
{
    @Singleton
    @Provides
    fun providesFakeApiApi(@Named(EmployeeDirRetrofitModule.TAG) retrofit: Retrofit) : EmployeeDirectoryApi =
            retrofit.create(EmployeeDirectoryApi::class.java)
}