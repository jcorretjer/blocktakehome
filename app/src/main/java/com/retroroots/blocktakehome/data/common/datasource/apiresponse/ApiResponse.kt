package com.retroroots.blocktakehome.data.common.datasource.apiresponse

sealed class ApiResponse<out S, out E>
{
    data class Success<out S, out E>(val data: S) : ApiResponse<S, E>()

    data class Error<out S, out E>(val error: E) : ApiResponse<S, E>()

    object Loading : ApiResponse<Nothing, Nothing>()
}
