package com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir

data class EmployeeDirectory(
    val employees: List<Employee> = listOf()
)
