package com.retroroots.blocktakehome.data.common.datasource.baseresponseprocessor

import retrofit2.Response

interface ApiResponseProcessor<P, out O>
{
    fun processResponse(process: Response<P>): O

    fun processException(exception: Throwable): O
}