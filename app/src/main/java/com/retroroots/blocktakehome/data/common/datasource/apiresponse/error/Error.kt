package com.retroroots.blocktakehome.data.common.datasource.apiresponse.error

import java.lang.Exception

interface Error
{
    val exception : Exception
}