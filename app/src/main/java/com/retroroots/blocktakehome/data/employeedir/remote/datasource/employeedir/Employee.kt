package com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeeresponse.EmployeeResponse
import kotlinx.parcelize.Parcelize

@Entity(tableName = "EmployeeTable")
@Parcelize
data class Employee(
    @SerializedName("biography")
    val biography: String = "",
    @SerializedName("email_address")
    val emailAddress: String?,
    @SerializedName("employee_type")
    val employeeType: Type,
    @SerializedName("full_name")
    val fullName: String?,
    @SerializedName("phone_number")
    val phoneNumber: String = "",
    @SerializedName("photo_url_large")
    val photoUrlLarge: String = "",
    @SerializedName("photo_url_small")
    val photoUrlSmall: String = "",
    @SerializedName("team")
    val team: String?,
    @SerializedName("uuid")
    val uuid: String,

    @PrimaryKey(autoGenerate = true)
    @Expose
    val id : Int = 0

) : Parcelable
{
    enum class Type(val value: String)
    {
        FULL_TIME("FULL_TIME"),
        PART_TIME("PART_TIME"),
        CONTRACTOR("CONTRACTOR");
    }

    companion object
    {
        fun toEmployee(employeeResponse: EmployeeResponse) : Employee? =

            employeeResponse.run {
                if(emailAddress == null || emailAddress.isEmpty())
                    return null

                if(fullName == null || fullName.isEmpty())
                    return null

                if(team == null || team.isEmpty())
                    return null

                return Employee(biography, emailAddress, employeeType, fullName, phoneNumber, photoUrlLarge, photoUrlSmall, team, uuid)
            }

    }
}