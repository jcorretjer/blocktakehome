package com.retroroots.blocktakehome.data.employeedir.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.Employee
import kotlinx.coroutines.flow.Flow

@Dao
interface EmployeeDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(employeeEntity: List<Employee>)

    @Query("SELECT * FROM employeetable")
    fun getEmployee() : List<Employee>
}