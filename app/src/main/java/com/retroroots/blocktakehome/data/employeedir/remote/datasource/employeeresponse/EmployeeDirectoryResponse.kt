package com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeeresponse

import com.google.gson.annotations.SerializedName

data class EmployeeDirectoryResponse(
    @SerializedName("employees")
    val employees: List<EmployeeResponse> = listOf()
)
