package com.retroroots.blocktakehome.data.employeedir.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.Employee

@Database(entities = [Employee::class], version = 2, exportSchema = false)
abstract class EmployeeDB : RoomDatabase()
{
    abstract fun employeeDao() : EmployeeDao
}