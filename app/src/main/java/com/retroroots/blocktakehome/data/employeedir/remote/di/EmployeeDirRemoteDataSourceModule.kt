package com.retroroots.blocktakehome.data.employeedir.remote.di

import com.retroroots.blocktakehome.data.employeedir.remote.api.EmployeeDirectoryApi
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.EmployeeDirRemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object EmployeeDirRemoteDataSourceModule
{
    @Singleton
    @Provides
    fun providesEmployeeDirRemoteDataSource(employeeDirectoryApi: EmployeeDirectoryApi) = EmployeeDirRemoteDataSource(employeeDirectoryApi)
}