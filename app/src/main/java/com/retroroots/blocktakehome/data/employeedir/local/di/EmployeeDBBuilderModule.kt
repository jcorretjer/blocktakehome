package com.retroroots.blocktakehome.data.employeedir.local.di

import android.content.Context
import androidx.room.Room
import com.retroroots.blocktakehome.data.employeedir.local.EmployeeDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object EmployeeDBBuilderModule
{
    @Singleton
    @Provides
    fun providesEmployeeBuilderDB(@ApplicationContext context: Context) =
            Room.databaseBuilder(context, EmployeeDB::class.java, "EmployeeDB")
                .fallbackToDestructiveMigration()
}