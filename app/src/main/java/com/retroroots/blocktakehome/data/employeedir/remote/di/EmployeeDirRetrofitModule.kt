package com.retroroots.blocktakehome.data.employeedir.remote.di

import com.retroroots.blocktakehome.data.employeedir.remote.api.EmployeeDirectoryApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object EmployeeDirRetrofitModule
{
    const val TAG = "EMPLOYEE_DIR"

    @Singleton
    @Provides
    @Named(TAG)
    fun providesFakeApiRetrofit() : Retrofit = Retrofit.Builder()
        .baseUrl(EmployeeDirectoryApi.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}