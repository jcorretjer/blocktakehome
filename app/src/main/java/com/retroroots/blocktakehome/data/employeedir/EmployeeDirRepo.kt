package com.retroroots.blocktakehome.data.employeedir

import com.retroroots.blocktakehome.data.common.datasource.apiresponse.ApiResponse
import com.retroroots.blocktakehome.data.common.datasource.apiresponse.error.ApiError
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.EmployeeDirRemoteDataSource
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.Employee
import com.retroroots.blocktakehome.data.employeedir.local.EmplyeeLocalDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class EmployeeDirRepo @Inject constructor(private val employeeDirRemoteDataSource: EmployeeDirRemoteDataSource, private val emplyeeLocalDataSource: EmplyeeLocalDataSource)
{
    suspend fun getAllEmployees() : Flow<ApiResponse<List<Employee>, ApiError>> =
            flow {

                emplyeeLocalDataSource.getEmployee().let { employees ->

                    if (employees.isEmpty())
                    {
                        employeeDirRemoteDataSource.getAllEmployees().let {

                            if (it is ApiResponse.Success)
                            {
                                emplyeeLocalDataSource.insertEmployee(it.data.employees)

                                emit(ApiResponse.Success(it.data.employees))
                            }

                            else if (it is ApiResponse.Error)
                            {
                                it.error.let { apiError ->

                                    when (apiError)
                                    {
                                        is ApiError.NoInternetConnectionError -> emit(ApiResponse.Error(ApiError.NoInternetConnectionError(apiError.exception)))

                                        is ApiError.ServerError -> emit(ApiResponse.Error(ApiError.ServerError(apiError.exception)))

                                        is ApiError.UnknownServerError -> emit(ApiResponse.Error(ApiError.UnknownServerError(apiError.exception)))
                                    }
                                }
                            }
                        }
                    }

                    else
                        emit(ApiResponse.Success(employees))
                }
            }


}