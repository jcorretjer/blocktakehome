package com.retroroots.blocktakehome.data.employeedir.remote.datasource

import com.retroroots.blocktakehome.data.common.datasource.baseresponseprocessor.ApiResponseProcessor
import com.retroroots.blocktakehome.data.common.datasource.apiresponse.ApiResponse
import com.retroroots.blocktakehome.data.common.datasource.apiresponse.error.ApiError
import com.retroroots.blocktakehome.data.employeedir.remote.api.EmployeeDirectoryApi
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.Employee
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.EmployeeDirectory
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeeresponse.EmployeeDirectoryResponse
import retrofit2.Response
import java.lang.Exception
import java.net.UnknownHostException

class EmployeeDirRemoteDataSource(val employeeDirectoryApi: EmployeeDirectoryApi) :
    ApiResponseProcessor<EmployeeDirectoryResponse,
            ApiResponse<EmployeeDirectory, ApiError>>
{
    suspend fun getAllEmployees(): ApiResponse<EmployeeDirectory, ApiError>
    {
        return try
        {
            processResponse(employeeDirectoryApi.getAllEmployees())
        }

        catch ( e : Throwable)
        {
            processException(e)
        }
    }

    override fun processResponse(process: Response<EmployeeDirectoryResponse>): ApiResponse<EmployeeDirectory, ApiError>
    {
        process.apply {
            return if (isSuccessful && body()!!.employees.isNotEmpty())
            {
                val employees = mutableListOf<Employee>().apply {

                    body()!!.employees.forEach {
                        Employee.toEmployee(it)?.let { employee ->
                            add(employee)
                        }
                    }
                }

                return ApiResponse.Success(EmployeeDirectory(employees))
            }

            else
                ApiResponse.Error(ApiError.ServerError(Exception(ApiError.SERVER_ERROR_MESSAGE)))
        }
    }

    override fun processException(exception: Throwable): ApiResponse<EmployeeDirectory, ApiError> =
            when(exception)
            {
                is UnknownHostException -> ApiResponse.Error(ApiError.NoInternetConnectionError(exception))

                else -> ApiResponse.Error(ApiError.UnknownServerError(exception as Exception))
            }
}