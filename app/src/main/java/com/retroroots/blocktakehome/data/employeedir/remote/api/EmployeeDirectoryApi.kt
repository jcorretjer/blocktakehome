package com.retroroots.blocktakehome.data.employeedir.remote.api

import com.retroroots.blocktakehome.BuildConfig
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeeresponse.EmployeeDirectoryResponse
import retrofit2.Response
import retrofit2.http.GET

interface EmployeeDirectoryApi
{
    companion object
    {
        const val BASE_URL = BuildConfig.EMPLOYEE_DIRECTORY_API_BASE_URL
    }

    @GET("employees.json")
    suspend fun getAllEmployees() : Response<EmployeeDirectoryResponse>
}