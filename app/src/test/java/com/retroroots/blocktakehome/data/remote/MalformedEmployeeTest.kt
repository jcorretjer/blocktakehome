package com.retroroots.blocktakehome.data.remote

import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeedir.Employee
import com.retroroots.blocktakehome.data.employeedir.remote.datasource.employeeresponse.EmployeeResponse
import junit.framework.TestCase.assertNull
import junit.framework.TestCase.assertTrue
import org.junit.Test

class MalformedEmployeeTest
{
    val fakeEmployees = listOf(
        EmployeeResponse(
            "bio",
            "email",
            Employee.Type.FULL_TIME,
            "name",
            "num",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/small.jpg",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/large.jpg",
            "team",
            "id"
        ),

        EmployeeResponse(
            "bio",
            null,
            Employee.Type.PART_TIME,
            null,
            "num",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/small.jpg",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/large.jpg",
            null,
            "id2"
        ),

        EmployeeResponse(
            "bio",
            "",
            Employee.Type.FULL_TIME,
            "",
            "num",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/small.jpg",
            "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/large.jpg",
            "",
            "i3"
        ),

        EmployeeResponse(
            biography =  "bio",
            employeeType = Employee.Type.PART_TIME,
            phoneNumber = "num",
            photoUrlLarge = "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/small.jpg",
            photoUrlSmall = "https://s3.amazonaws.com/sq-mobile-interview/photos/16c00560-6dd3-4af4-97a6-d4754e7f2394/large.jpg",
            uuid = "id5"
        )
    )

    @Test
    fun validateItemsAreExcludedTest()
    {
        assertNull(Employee.toEmployee(fakeEmployees[1]))

        assertNull(Employee.toEmployee(fakeEmployees[2]))

        assertNull(Employee.toEmployee(fakeEmployees[3]))
    }

    @Test
    fun validateItemsAreIncludedTest()
    {
        assertTrue(Employee.toEmployee(fakeEmployees[0]) != null)
    }
}